const test = require('ava');

function scaffoldStructure(document, data) {
  document.documentElement.innerHTML = '';
  const ul = document.createElement('ul');
  data.forEach(entry => {
      const li = document.createElement('li');
      const name = document.createElement('span');
      name.innerHTML = entry.name;
      name.className = 'name';
      name.style = 'font-weight: bold';
      li.append(name);
      const separator = document.createElement('span');
      separator.innerHTML = ' - ';
      li.append(separator);
      const email = document.createElement('span');
      email.innerHTML = entry.email;
      li.append(email);
      ul.appendChild(li);
  })
  document.body.appendChild(ul);
}

const data = [
  { name: 'Bernardo', email: 'b.ern@mail.com' },
  { name: 'Carla', email: 'carl@mail.com' },
  { name: 'Fabíola', email: 'fabi@mail.com' }
];

// test.before creates entire HTML at once so that the subsequent test() methods don't need to build it again 
test.before(t => scaffoldStructure(document, data))

test('has an unordered list', t => {
  const hasUL = document.getElementsByTagName('ul').length > 0
  t.is(hasUL, true);
})

test('has three .name classes', t => {
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
})

test('has ordered text in alphabetical order', t => {
  const nameNodes = document.querySelectorAll('.name');
  let names = []
  nameNodes.forEach(node => names.push(node.innerHTML))

  const expectedNames = data.reduce((total, cur) => {
      total.push(cur.name);
      return total;
  }, []).sort((a, b) => a < b ? -1 : 1)

  t.deepEqual(names, expectedNames);
})

test('has styled elements', t => {
  const nameNodes = document.querySelectorAll('.name');
  let allBold = 1;
  nameNodes.forEach(node => allBold &= node.style['font-weight'] === 'bold')
  t.is(allBold, 1);
})
