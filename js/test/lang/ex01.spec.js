function objectFromArrays(keys, values) {
  return keys.reduce((total, cur, index) => {
      total[cur] = values[index];
      return total;
  }, {})
}

test('Creating objects from arrays', () => {
  const keys = ['name', 'email', 'username'];
  const values = ['Bruno', 'bruno@mail.com', 'bruno'];

  const finalObject = {
    name: 'Bruno',
    email: 'bruno@mail.com',
    username: 'bruno'
  };

  expect(objectFromArrays(keys, values)).toEqual(finalObject);
});

function reverse(a, b) {
  return a < b ? 1 : -1;
}

function indexer(name, index) {
  return `${index + 1}. ${name}`;
}

function shorterThan6(name) {
  return name.length < 6;
}

describe('array functions', () => {
  test('reversed indexed arrays', () => {
    const names = ['Marina', 'Camila', 'Alberto', 'Felipe', 'Mariana'];

    const test = names.sort(reverse).map(indexer);

    const correct = [
      '1. Marina',
      '2. Mariana',
      '3. Felipe',
      '4. Camila',
      '5. Alberto'
    ];
    expect(test).toEqual(correct);
  });

  test('filtering lists', () => {
    const techComps = [
      'microsoft',
      'google',
      'apple',
      'ibm',
      'amazon',
      'facebook'
    ];

    const shortNames = techComps.filter(shorterThan6);
    const correctShortNames = ['apple', 'ibm'];

    expect(shortNames).toEqual(correctShortNames);
  });
});
