function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  test("user's name size", () => {
    expect(() => createUser('', 'pedro@gmail.com.br', 'senha123', 'senha123')).toThrow(Error);
    expect(() => createUser('pedropedropedropedropedropedropedropedropedropedropedropedropedropedro', 'pedro@gmail.com.br', 'senha123', 'senha123')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br.br', 'senha123', 'senha123')).not.toThrow(Error);
  })
  test("user's email format", () => {
    expect(() => createUser('pedro', 'pedro_12!!22 3 ~@gmail.com.br', 'senha123', 'senha123')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha123', 'senha123')).not.toThrow(Error);
  })
  test("user's password format", () => {
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha', 'senha')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha!!!!~@', 'senha!!!!~@')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha123', 'senha123')).not.toThrow(Error);
  })
  test("user's passwords match", () => {
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha123', 'senha1233')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha1233', 'senha123')).toThrow(Error);
    expect(() => createUser('pedro', 'pedro@gmail.com.br', 'senha123', 'senha123')).not.toThrow(Error);
  })
});
